export class DataTableConstants {
  public static usuariosPendencias: string[] = ['renan.garcia', 'alisson', 'zoe', 'pop', 'evelin', 'daniela.silva', 'manoel.santana'];
  public static PageSize: number[] = [10, 50, 100, 200, 500];
  public static AllowFiltering: boolean = true;
}
