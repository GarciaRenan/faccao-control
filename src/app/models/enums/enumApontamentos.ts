export enum ApontamentoList {
  'Em transporte',
  'Não informado',
  'Em fila',
  'Em produção',
  'Parado',
  'Em inspeção',
  'Disponível para coleta',
  'Coletado',
  'Não industrializado'
}

export enum ApontamentoListParado {
  'Em transporte',
  'Não informado',
  'Em fila',
  'Em produção',
  'Parado',
  'Em inspeção',
  'Disponível para coleta',
  'Coletado',
  'Não industrializado',
  'Parado - Despriorizado',
  'Parado - Resolução de pendência',
  'Parado - Remanejamento',
  'Parado - Sistema'
}
